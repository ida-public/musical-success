function Y=smoothbins(X,N,sigma)

if (length(N)==1) && (N==1)
    Y=X;
    
else
    % check for odd number in filter size
    for k=1:length(N)
        if mod((N(k)+1),2)>0.4
            disp('Warning : Invalid Filter Length')
            Y=X;
            return
        end
    end
    %small add from Boris:replicate edge values to avoid edge effect
    for k=1:length(N)
        n_r(k)=round((N(k)-1)/2);
    end;
    if length(N)==1,
        n_r(2)=n_r(1);
    end;
    %n_r=fliplr(n_r);
    if length(N)<=2
        X=[X(repmat(1,n_r(1),1),:,:);X;X(repmat(end,n_r(1),1),:,:)];
        X=[X(:,repmat(1,n_r(2),1),:) X X(:,repmat(end,n_r(2),1),:)];
    end;
    
    if length(N)==1
        shift1=(N-1)/2;
        shift2=shift1;
        if nargin<3,%uniform filter
            fil=ones(N,N)/(N*N);
        else,%gaussian filter
            ind = -floor(N/2) : floor(N/2);
            [Xi Yi] = meshgrid(ind, ind);
            
            %// Create Gaussian Mask
            h = exp(-(Xi.^2 + Yi.^2) / (2*sigma*sigma));
            
            %// Normalize so that total area (sum of all weights) is 1
            fil = h / sum(h(:));
        end;
    elseif length(N)==2
        shift1=(N(1)-1)/2;
        shift2=(N(2)-1)/2;
        if nargin<3,%uniform filter
        fil=ones(N(1),N(2))/(N(1)*N(2));
        else,%gaussian filter
            ind = -floor(N(1)/2) : floor(N(1)/2);
            ind2 = -floor(N(2)/2) : floor(N(2)/2);
            [X Y] = meshgrid(ind, ind2);
            
            %// Create Gaussian Mask
            h = exp(-(X.^2 + Y.^2) / (2*sigma*sigma));
            
            %// Normalize so that total area (sum of all weights) is 1
            fil = h / sum(h(:));
        end;
    else
        disp('Warning : Invalid Filter Size')
        Y=X;
        return
    end
    
    [m,n,p]=size(X);
    if m>0
        if isa(X,'single'),
            Y=zeros(m,n,p,'single');
        else
            Y=zeros(m,n,p);
        end;
        for k=1:p
            temp=X(:,:,k);
            [m,n]=size(temp);
            temp=conv2(temp,fil);
            Y(:,:,k)=temp(((1:m)+shift1 ),((1:n)+shift2 ));
        end
        %small add from Boris:remove the added edges
%         Y=Y(1+n_r(1):end-n_r(1),1+n_r(2):end-n_r(2),:);
        Y([1:n_r(1) (end-n_r(1)+1):end],:,:)=[];
        Y(:,[1:n_r(2) (end-n_r(2)+1:end)],:)=[];
    else
        Y=[];
    end
end

