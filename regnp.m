function [f,newx,abscisse] = regnp(x,y,h,nbre,graph)
%REGNP   r�gression non-param�trique � noyau gaussien
%
%USAGE : [f,newx,abscisse] = regnp(x,y,h,nbre,graph)
%
%INPUT : x,y - vecteurs colonne (de meme taille)
%        h - largeur de la fenetre de la r�gression non-param�trique (par d�faut
%            1/50 * amplitude des valeurs de x)
%        nbpts - nombre de points (400 par d�faut)
%        graph - repr�sentation graphique des r�sultats (1 si oui (d�faut), 0 si
%                non)
%
%OUTPUT : f - courbe de r�gression non-lin�aire
%         newx - estimateur de y
%         abscisse - abscisse de la courbe de r�gression non-lin�aire
%
%[Rao 65] : "Linear Statistical Inference and its Applications", John Wiley
%and Sons, 1965

if nargin<5,
    graph=1;
end;
if nargin<4,
    nbre=400;
end;

x=x(:);
y=y(:);
index=~isnan(x)&~isnan(y);
if sum(index)<length(x),
    warning('Your data is dirty ! Some Nan');
end;
x=x(index);
y=y(index);
if nargin<3,
    h=range(x(:))/50;
end;
[rx cx]=size(x);
[ry cy]=size(y);
pas=(max(x)-min(x))/nbre;
abscisse=(min(x)-2*pas:pas:max(x)+2*pas);
[a nbrepts]=size(abscisse);
k=normpdf((single(x(:,ones(1,nbrepts)))-single(abscisse(ones(rx,1),:)))/h);
f=(y'*k)./sum(k);
if graph
    figure;plot(abscisse,f,'r');
    hold;
    plot(x,y,'.');
    figure;plot(x);
end;
for i=1:rx,
   avant=(abscisse<=x(i));%on cherche l'indice dans la fonction correspondant � la valeur recherch�e
	avant=sum(avant);   
   newx(i)=1/2*(f(avant)+f(avant+1));%on prend la moyenne entre la valeur � cet indice et celui d'apr�s
end;
if graph,
    hold;
    plot(newx,'r');
    plot(y,'g');
    legend('x','y estim�','y initial');
end;