% BILLBOARD 200 database analysis.
% Paper by Gourevitch, 2023
% Code compatible with Matlab 2019b

%% %%%%%%%%%%%%%%%%%% Local Variables
clear
% Variable to be adapted to your path
global YOUR_WORKSPACE_FOLDER
YOUR_WORKSPACE_FOLDER='D:\Dropbox\Boris\Musique_etude';
cd(YOUR_WORKSPACE_FOLDER)

%following sections "read SQL database" and "precomputations" are to show computations done, 
%you can directly go to the section "load data" to reproduce figures
%sections with "Bonus" add figures unshown in the paper
%% %%%%%%%%%%%%%%%%%% Read SQL database

%% import BILLBOARD 200 database 

file='billboard-200-with-segments.db';%put this file in your workspace folder. Description and downloadable database at https://components.one/datasets/billboard-200-with-segments
% description is written here:
% A larger version of the Billboard 200 database. The first two tables are included in the smaller version of the database:
% 574,000 rows containing all albums in the Billboard 200 from 1/5/1963 to 1/19/2019.
% Each row contains the album�s place in the charts, the week of the chart, album name, artist name, and where available from Spotify and not null in the table, the number of tracks, and length of the album in milliseconds. This table is titled �albums�.
% 340,000 rows containing acoustic data for tracks from Billboard 200 albums from 1/5/1963 to 1/19/2019.
% Each row contains track ID on Spotify, track name, album name, artist name, values for Spotify EchoNest acoustic data (acousticness, danceability, energy, instrumentalness, liveness, loudness, speechiness, key, time signature, and valence), duration in milliseconds, album ID on Spotify, and release date of the album. Contains no null values. This table is titled �acoustic_features�.
% 2,821,061 rows of segment data from songs on Billboard 200 albums. Each song on Spotify typically contains upwards of 1000 segments; this table is constituted from a sample of 20 of those segments for each song or the max number of segments, whichever is smaller.
% Each row contains track ID on Spotify, track name, album name, artist name, the album ID on Spotify, 12-dimensional timbral data, confidence value, duration of the segment, max loudness, time of max loudness, and the start time of max loudness.
% For more information on these values, see Spotify�s documentation on segments.

%use sqlite4java builtin function of matlab
db = com.almworks.sqlite4java.SQLiteConnection(java.io.File(file));
db.open;

% Prepare an SQL query statement
disp('extracting ALBUMS');
stmt = db.prepare(['select * from albums']);%put everything in stmt

% Step through the result set rows
row = 1;
table_albums=nan(573947,7);
table_albums_s{573947,3}=[];
stmt.step;%remove first line
while stmt.step
    for j=[1 5 6 7],%stmt.columnCount,
        table_albums(row,j) = stmt.columnDouble(j-1);    % column #0
    end;
    for j=[2 3 4],%stmt.columnCount,
        if isjava(stmt.columnString(j-1))
            table_albums_s{row,j-1} = toCharArray(stmt.columnString(j-1))';    % column #0
        else
            table_albums_s{row,j-1} = stmt.columnString(j-1);    % column #0
        end;
    end;
    row=row+1
    if row/1000-floor(row/1000)==0,%display rows numbers every 1000
        row
    end;
end
stmt.dispose

% remove last line
table_albums_s(end,:)=[];
table_albums(end,:)=[];

%variables of table_album_s: {week of the chart, album name, artist name}
%variables of table_album: (ID,rank in the charts,number of tracks,length in ms}

%compute score
score=201-table_albums(:,5);

%create variables of years, months and days
N=size(table_albums_s,1);
Y=nan(N,1);
M=Y;
D=Y;
for i=1:N-1
    [Y(i),M(i),D(i)] = datevec(table_albums_s{i,1});
    if i/1000-floor(i/1000)==0,%display rows numbers every 1000
        i
    end;
end;
%index of weeks since first week
Days_1963=nan(N,1);
for i=1:N-1
    [Days_1963(i)] = datenum(table_albums_s{i,1});
    if i/1000-floor(i/1000)==0,
        i
    end;
end;

[~,~,Weeks_idx]=unique(Days_1963);

save database_matlab

%% import BILLBOARD 100 database

hot100_db = readtable('billboard-100.csv','Delimiter',',','Encoding','UTF-8','PreserveVariableNames',1);
hot100_db=table2cell(hot100_db);
hot100_var_names={'URL','WeekID','Week position','Song','Performer','SongID','Instance',...
    'Previous Week Position','Peak Position',' Weeks on Chart'};

%same variables as for BB200
score_100=101-cell2mat(hot100_db(:,3));
table_albums_s_100=hot100_db(:,[2 5 4]);
N_100=size(table_albums_s_100,1);
Y_100=nan(N_100,1);
M_100=Y_100;
D_100=Y_100;
for i=1:N_100-1
    [Y_100(i),M_100(i),D_100(i)] = datevec(table_albums_s_100{i,1});
    if i/1000-floor(i/1000)==0,
        i
    end;
end;
save database_matlab table_albums_s_100 score_100 Y_100 M_100 D_100 -append;

%% %%%%%%%%%%%%%%%%%% PRE-COMPUTATIONS

%list of artists
[all_artists,ia,ic]=unique(table_albums_s(:,2));
%cumulated score for each artist
pts_all_artists = accumarray(ic,score,[],@sum);

%remove a few albums that are not from one artist
[Lia,Locb] =ismember(all_artists,{'Soundtrack','Various Artists','Original Cast'});
artists_to_use=setdiff(1:length(all_artists),[1;find(Lia)]);

%sales of an album as a function of the distance in YEARS with the previous one
success_years_interv_arch=[];
success_years_arch=[];
score_success_ref_arch=[];
for i=artists_to_use,
    i
    %years of albums
    idx=strcmp(table_albums_s(:,2),all_artists{i});
    distrib_year=accumarray(Y(idx),score(idx),[],@sum);
    success_years=find(distrib_year);
    if length(success_years)>1,
        success_years_interv=diff(success_years);
        score_success=distrib_year(find(distrib_year));
        score_success_ref=score_success(2:end)./score_success(1:end-1);
        success_years_interv_arch=[success_years_interv_arch;success_years_interv(:)];
        success_years_arch=[success_years_arch;success_years(2:end)];
        score_success_ref_arch=[score_success_ref_arch;score_success_ref(:)];
    end;
end;

%sales of an album as a function of the distance in WEEKS with the previous one
success_weeks_interv_arch=[];
score_success_ref_weeks_arch=[];
success_weeks_arch=[];
artist_weeks_arch=[];
for i=artists_to_use,
    i
    %years of albums
    idx=strcmp(table_albums_s(:,2),all_artists{i});
    %remove duplicates
    [a,b,c]=unique(table_albums_s(idx,3));
    weeks_temp=accumarray(c,Weeks_idx(idx),[],@min);    
    weeks_temp=sort(weeks_temp);
    if length(weeks_temp)>1,
        success_weeks_interv=diff(weeks_temp);
        score_success_ref=weeks_temp(2:end)./weeks_temp(1:end-1);
        success_weeks_interv_arch=[success_weeks_interv_arch;success_weeks_interv(:)];
        success_weeks_arch=[success_weeks_arch;weeks_temp(2:end)];
        artist_weeks_arch=[artist_weeks_arch;i*ones(length(weeks_temp)-1,1) (2:length(weeks_temp))'];
        score_success_ref_weeks_arch=[score_success_ref_weeks_arch;score_success_ref(:)];
    end;
end;
% success_weeks_interv_arch=abs(success_weeks_interv_arch);

%create a database of albums with dates of charts and scores, for each artist
clear database_artists database_artists_r
for i=artists_to_use,
    i
    
    %years of albums
    idx=strcmp(table_albums_s(:,2),all_artists{i});
    %list of albums
    albums=unique(table_albums_s(idx,3));
    for j=1:length(albums),
        idx_al=idx&strcmp(table_albums_s(:,3),albums{j});
        database_artists{i}{j}=sortrows([Weeks_idx(idx_al) score(idx_al)],'ascend');
        if database_artists{i}{j}(1,1)>2&database_artists{i}{j}(1,1)<30,
            error('truc');
        end;
        database_artists_r{i}(j,:)=[database_artists{i}{j}(1,1) sum(idx_al) range(database_artists{i}{j}(:,1))+1 max(database_artists{i}{j}(:,2)) sum(database_artists{i}{j}(:,2))];
    end;
    %sort albums per date
    if length(albums)>1,
        [~,idx_sort]=sort(database_artists_r{i}(:,1));
        database_artists{i}=database_artists{i}(idx_sort);
        database_artists_r{i}=database_artists_r{i}(idx_sort,:);
    end;
end;

%variables in database_artists_r
var_database_artists_r={'week_start','nb_weeks','range_weeks','max_score','cum_score'};

% same database under another shape (artist,n-th album of the artist,variables in database_artists_r)
maxi=0;
for i=artists_to_use,
    i
    maxi=max(maxi,size(database_artists_r{i},1));
end;
database_artists_archive=nan(length(database_artists_r),maxi,5);
for i=artists_to_use,
    i
    database_artists_archive(i,1:size(database_artists_r{i},1),:)=database_artists_r{i};
end;

save database_matlab_analysis success_weeks_interv_arch success_weeks_arch artist_weeks_arch database_artists_archive score_success_ref_weeks_arch;
save database_matlab_analysis all_artists pts_all_artists success_years_interv_arch score_success_ref_arch -append
save database_matlab_analysis artists_to_use success_years_arch database_artists database_artists_r -append

%Success duration (in years) and peak lag for a given artist
success_database_artist=nan(size(all_artists,1),5);
for i=artists_to_use,
    i
    %years of albums
    idx=strcmp(table_albums_s(:,2),all_artists{i});
    distrib_year=accumarray(Y(idx),score(idx),[],@sum);
    success_years=find(distrib_year);
    [success_peak success_peak_year]=max(distrib_year);
    success_database_artist(i,:)=[success_years(1) success_years(end) range(success_years)+1 ...
        success_peak(1) success_peak_year(1)-success_years(1)+1];
end;
%variables in success_database_artist (in years)
var_success_database_artist={'Beginning','End','Duration','Peak_Score','Peak_Lag'};

%nb weeks in the charts for a given artist
success_database_artist_weeks=nan(max(artists_to_use),1);
for i=artists_to_use,
    i
    %years of albums
    idx=strcmp(table_albums_s(:,2),all_artists{i});
    success_database_artist_weeks(i)=sum(idx);
end;

save database_matlab_analysis success_database_artist_weeks success_database_artist var_success_database_artist -append

%% %%%%%%%%%%%%%%%%%% ANALYSIS

%% Load data

load database_matlab
load database_matlab_analysis

%% MUSICAL SUCCESS IS PRECARIOUS

%number of different albums for a given year, 2000 here

idx2=find(Y==2000);
idx=find(Y<2000);
all_albums=table_albums_s(idx2,3);
[all_albums,ia,ic]=unique(all_albums);
idx_unique_2000=idx2(ia);
[all_albums,ia2] =setdiff(all_albums,unique(table_albums_s(idx,3)));
idx_unique_2000=idx_unique_2000(ia2);
%number of albums this year
length(idx_unique_2000)

all_artists_2000=table_albums_s(idx_unique_2000,2);
all_artists_2000=unique(all_artists_2000);
%number of artists having charted this year
length(all_artists_2000)

%percentage of artists having already charted before
new_artists_2000=setdiff(all_artists_2000,unique(table_albums_s(idx,2)));
round((length(all_artists_2000)-length(new_artists_2000))/length(all_artists_2000)*100)

%number of artists having only one album in the charts
sum(isnan(database_artists_archive(:,2,1)))/size(database_artists_archive,1)

%which artists lasted more than 48 years
idx=find(success_database_artist(:,3)>=48);
temp=success_database_artist(idx,:);
[B,idx_sort]=sortrows(temp,[3 4],'descend');

%display: artist, beginning, last year in the charts, number of years, total score
truc=[char(all_artists(idx(idx_sort))) num2str(temp(idx_sort,1:4))]

%output: write an excel database with the list of artists and cumulated scores
xlswrite('table_longest_carreers.xls',temp(idx_sort,1:4),'B2:E98')
xlswrite('table_longest_carreers.xls',all_artists(idx(idx_sort)),'A2:A98')

%% FIGURE 1A: Distribution of the total score for each artist measured over their entire career

figure;
hold on;
plot(sort(log10(pts_all_artists(artists_to_use))),(1:length(artists_to_use))/length(artists_to_use),'.');
%gaussian fit
x1=sort(log10(pts_all_artists(artists_to_use)));
y1=(1:length(artists_to_use))/length(artists_to_use);
modelfun_gaussian = @(b,x)(1/2+1/2*erf((x(:)-b(2))/b(1)));

[beta,R,J,CovB,MSE,ErrorModelInfo] = nlinfit(x1(:),y1(:),modelfun_gaussian,[3 3]);

plot(0.1:0.1:6,modelfun_gaussian(beta,0.1:0.1:6),'r:','linewidth',2);
set(gca,'xtick',0:2:6,'xticklabel',10.^(0:2:6),'ytick',0:0.5:1,'yticklabel',0:50:100)

%additional figures
%distribution
figure;
hist((x1-beta(2))/beta(1),100)
%test against gaussian distribution
[h,p,jbstat,critval] = jbtest((x1-beta(2))/beta(1))
%compute kurtosis
kurtosis((x1-beta(2))/beta(1))
%Kolmogorov-Smirnov test
[h,p,ksstat,cv] =kstest((x1-beta(2))/beta(1))
%QQ plot
figure;
[pdca] = fitdist(x1+0.01,'normal');
qqplot(x1,pdca)
%alternative version
figure;
qqplot(zscore(log10(pts_all_artists(artists_to_use))))

%% FIGURE 1B: Number of albums appearing in the BB200 per artist

%logarithmic law fit
modelfun_logd = @(b,x)(b(2)*(b(1).^x(:))./x(:));

cmap=lines(10);

figure;
temp=sum(~isnan(database_artists_archive(:,1:end-1,1))&isnan(database_artists_archive(:,2:end,1)))/size(database_artists_archive,1)*100;
h=bar(temp);
set(h,'edgecolor','none','facecolor',[0.6 0.6 1])
hold on;
set(gca,'xscale','linear','yscale','log')
xlim([0 70]);
ylim([8*10^-3 100])
set(gca,'ytick',[10.^(-2:2)])

n_alb=27;%fit up to a certain number of albums, percentages are too small thereafter
[beta,R,J,CovB,MSE,ErrorModelInfo] = nlinfit((1:n_alb)',temp(1:n_alb)',modelfun_logd,[0.9 50],'weights',(1:n_alb)'.^3);%[ones(5,1);1*ones(n_years-5,1)]);
area(modelfun_logd([beta],1:n_alb),'facecolor','r','facealpha',0.2,'edgecolor','none');
plot(modelfun_logd([beta],1:n_alb),'r-')

%% FIGURE 1C: Duration of artists' success measured as the number of years between their first and last appearance on the BB200

a=hist(success_database_artist(:,3),0:57);

figure('position',[560   804   279   144]);
hold on;
h1=bar(0:57,a/sum(a)*100,'facecolor',[0.6 0.6 1]);
set(h1,'edgecolor','none');
h3=plot(0:57,fliplr(cumsum(fliplr(a/sum(a)*100))),'linewidth',2);

set(gca,'xscale','linear','yscale','log','ytick',[0.1 1 10 100],'yticklabel',[0.1 1 10 100])
set(gca,'xtick',[1 10:10:50])
ylim([0.025 100])
xlim([0 58])
xlabel('years');
ylabel('% artists');
legend({'Success duration','Success \geq n years'},'interpreter','tex');
legend('boxoff')

%% FIGURE 1D: Number of #1 albums per artist.

idx=sum(database_artists_archive(:,:,4)==200,2)>0&~isnan(database_artists_archive(:,10,4));%200 is rank 1
nb_1=nansum((database_artists_archive(idx,:,4)==200),2);
a=hist(nb_1,1:20);

figure('position',[757  400  200   150]);
bar(1:20,a/sum(a),'facecolor',[0.6 0.6 1],'edgecolor','none','barwidth',1)
set(gca,'xscale','linear','box','off','ticklength',[0 0])
hold on
plot(1:1:20,smoothbins(a/sum(a),3),'k:','linewidth',2)

%yule law
t=1:200;
sumt=sum(1./(t.*(t+1)));
cmap=lines(10);
plot(t,1./(t.*(t+1))/sumt,'color',cmap(2,:),'linewidth',2)
xlim([0 20])
set(gca,'yscale','log')
xlabel('N albums #1');
ylabel('% artists');

%% FIGURE 1E: Number of weeks spent at #1 on the BB200 by artist.

idx=sum(database_artists_archive(:,:,4)==200,2)>0&~isnan(database_artists_archive(:,10,4));%200 is rank 1
nb_weeks_1=nansum((database_artists_archive(idx,:,4)==200).*database_artists_archive(idx,:,2),2);
a=hist(nb_weeks_1,5:10:2055);

figure('position',[757  400  200   150]);
bar(5:10:2055,a,'facecolor',[0.6 0.6 1],'edgecolor','none','barwidth',1)
set(gca,'xscale','linear','box','off','ticklength',[0 0])
xlim([0 1000])
hold on
plot(5:10:2055,smoothbins(a,5),'k:','linewidth',2)
xlabel('N weeks #1');
ylabel('N artists');

%% Bonus: which artists are the most successfull ?

[B,I] = sort(pts_all_artists,'descend');
all_artists(I(1:100))

figure;
hist(log10(pts_all_artists(artists_to_use)),300);
set(gca,'xscale','log','yscale','log');

%% FIGURE 2A + FIGURE 3Ai, Di + SUPP FIGURE 1 & 2: Cumulative score of an artist represented by year

% artist={'Billy Joel'};
% artist={'Madonna'};
% artist={'Adele'};
% artist={'Prince'};
% artist={'Michael Jackson'};
% artist={'Barbra Streisand'};
% artist={'Garth Brooks'};
% artist={'Queen'};
% artist={'Elton John'};
% artist={'Bob Marley And The Wailers'};
% artist={'Marvin Gaye'};
% artist={'James Brown'};
% artist={'Frank Sinatra'};
% artist={'Mariah Carey'};
% artist={'The Beatles'};
% artist={'Bruce Springsteen';
% artist={'Stevie Wonder'};
artist={'The Rolling Stones'};

distrib_choice='frechet';%frechet or weibull

years_ref=1963:2019;

figure('position',[200   800   217   145]);
hold on;

for j=1:length(artist),
    idx=strcmp(table_albums_s(:,2),artist{j});
    distrib_year=accumarray(Y(idx),score(idx),[],@sum);
    if length(distrib_year)<2019,
        distrib_year(2019)=0;
    end;
    distrib_year=distrib_year(1963:2019);
    
    bar(years_ref,distrib_year,'facecolor',[0.6 0.6 1],'edgecolor','none')
    y=smoothbins(distrib_year,5);
    y(y==0)=nan;
    plot(years_ref,y,'linewidth',2)
end;
set(gca,'xtick',[1963 1980 2000 2020])
xlabel('year');
ylabel('score');
y(isnan(y))=0;
start=find(y,1);

n_years=length(years_ref(start:end));

% weibull's law fitting
modelfun_w = @(b,x)(b(3)*(x(:)).^(b(2)-1).*exp(-(x(:).^b(2))/b(1)));

% other possible models
% modelfun_gamma = @(b,x)(b(3)*x(:).^(b(2)-1).*exp(-(x(:)/b(1))));
modelfun_frechet = @(b,x)(b(3)*x(:).^(-b(2)-1).*exp(-(x(:)/b(1)).^-b(2)));
% modelfun_sigm = @(b,x)(((x-b(1))<0)./(1+exp(b(2)*(x-b(1))))+((x-b(1))>=0)./(1+exp(b(3)*(x-b(1)))));
% modelfun_bilinear = @(b,x)(((x-b(1))<=0).*x.*b(2)+((x-b(1))>0).*(b(3).*(x-b(1))+b(1)*b(2)));


switch distrib_choice
    case 'frechet',
        
        % % [beta,R,J,CovB,MSE,ErrorModelInfo] = nlinfit((1:n_years+1)',[0;y(start:end)],modelfun_gamma,[5 0.5 max(y)]);%,'weights',1./(1:n_years+1)');%[ones(5,1);1*ones(n_years-5,1)]);
        % [beta,R,J,CovB,MSE,ErrorModelInfo] = nlinfit((0:n_years)',[0;y(start:end)/max(y)],modelfun_bilinear,[4 0.25 -0.1]);%,'weights',1./(1:n_years+1)');%[ones(5,1);1*ones(n_years-5,1)]);
        [beta,R,J,CovB,MSE,ErrorModelInfo] = nlinfit((1:n_years)',[y(start:end)],modelfun_frechet,[10 0.8 max(y(start:end))*10]);%,'weights',1./(1:n_years+1)');%[ones(5,1);1*ones(n_years-5,1)]);
        plot(1962+(start:start+n_years-1),modelfun_frechet(beta,1:n_years),'k:','linewidth',2);
    case 'weibull',
        [beta,R,J,CovB,MSE,ErrorModelInfo] = nlinfit((1:n_years)',[y(start:end)],modelfun_w,[20 0.9 max(y(start:end))]);%,'weights',y(start:end));%1./(1:n_years)');%[ones(5,1);1*ones(n_years-5,1)]);
        plot(1962+(start:start+n_years-1),modelfun_w(beta,1:n_years),'k:','linewidth',2);
end;

%% FIGURE 2A (Bonus): results month by month

% artist='The Rolling Stones';
% artist='Billy Joel';
% artist='Madonna';
% artist='Adele';
% artist='Prince';
artist='Michael Jackson';
% artist='Barbara Streisand';
% artist='Garth Brooks';
% artist='Mariah Carey';
% artist='The Beatles';
% artist='Bruce Springsteen';
% artist='Stevie Wonder';
idx=strcmp(table_albums_s(:,2),artist);

years_ref=1963:2019;
distrib_month=accumarray([Y(idx) M(idx)],score(idx),[],@sum);
if length(distrib_month)<2019,
    distrib_month(2019,:)=0;
end;
if size(distrib_month,2)<12,
    distrib_month(:,12)=0;
end;
distrib_month=distrib_month(1963:2019,:);
distrib_month=distrib_month';

figure('position',[129         725        1492         199]);
hold on;
title(artist)
bar(1963+1/12:1/12:2020,distrib_month(:))
% y=smoothbins(distrib_month(:),5);
% plot(1963+1/12:1/12:2020,y,'linewidth',1)
set(gca,'xtick',1963:2019,'xticklabelrotation',90);
xlabel('month');
ylabel('score');

%% FIGURE 2B: Success for a set of artists

% artist='The Rolling Stones';
% artist='Billy Joel';
% artist='Madonna';
% artist='Adele';
% artist='Prince';
% artist='Michael Jackson';
% artist='Barbara Streisand';
% artist='Garth Brooks';
% artist='Mariah Carey';
% artist='The Beatles';
% artist='Bruce Springsteen';
% artist='Stevie Wonder';
% artist='John Coltrane';
% artist={'The Rolling Stones'};
artist={'Bob Dylan','Barbra Streisand','Elton John','Billy Joel','Garth Brooks','Mariah Carey'};

figure('position',[560   796   345   152]);
hold on;

for j=1:length(artist),
    idx=strcmp(table_albums_s(:,2),artist{j});
    distrib_year=accumarray(Y(idx),score(idx),[],@sum);
    if length(distrib_year)<2019,
        distrib_year(2019)=0;
    end;
    distrib_year=distrib_year(1963:2019);
    
    y=smoothbins(distrib_year,5);
    y(y==0)=nan;
    plot(1963:2019,y,'linewidth',2)
end;
set(gca,'xtick',[1963 1980 2000 2020])
legend(artist,'location','east')
legend('boxoff')
xlabel('year');
ylabel('score');



%% FIGURE 2C: years of the career where the peak arrives

idx_years=0:57;%from 1962 to 2019

%probability density function
figure('position',[560   804   279   144]);

idx=success_database_artist(:,3)<10;%at least 10 years of success
a=hist(success_database_artist(idx,5),idx_years);

idx=success_database_artist(:,3)>=10&success_database_artist(:,3)<20;%at least 10 years of success
b=hist(success_database_artist(idx,5),idx_years);

idx=success_database_artist(:,3)>=20;%at least 10 years of success
c=hist(success_database_artist(idx,5),idx_years);

hold on;
h1=plot(idx_years,a/sum(a)*100,'linewidth',2);
h2=plot(idx_years,b/sum(b)*100,'linewidth',2);
h3=plot(idx_years,c/sum(c)*100,'linewidth',2);

set(gca,'xscale','linear','yscale','log','ytick',[0.1 1 10 100],'yticklabel',[0.1 1 10 100])
set(gca,'xtick',[1 10:10:50])

xlim([0 30])
xlabel('years');
ylabel('% artists');
legend({'Career < 10y','10y \leq Career < 20y','Career \geq 20y'},'interpreter','tex');
legend('boxoff')

%cumulative density function as in Fig. 3C
figure('position',[560   804   279   144]);

idx=success_database_artist(:,3)>=3&success_database_artist(:,3)<10;%at least 10 years of success
a=hist(success_database_artist(idx,5),idx_years);

idx=success_database_artist(:,3)>=10&success_database_artist(:,3)<20;%at least 10 years of success
b=hist(success_database_artist(idx,5),idx_years);

idx=success_database_artist(:,3)>=20;%at least 10 years of success
c=hist(success_database_artist(idx,5),idx_years);

hold on;
h1=plot(idx_years,cumsum(a)/sum(a)*100,'linewidth',2);
h2=plot(idx_years,cumsum(b)/sum(b)*100,'linewidth',2);
h3=plot(idx_years,cumsum(c)/sum(c)*100,'linewidth',2);

set(gca,'xscale','linear')
set(gca,'xtick',[1 10:10:50])
xlim([0 30])
xlabel('years');
ylabel('% artists');
legend({'Career < 10y','10y \leq Career < 20y','Career \geq 20y'},'interpreter','tex','location','southeast');
legend('boxoff')

%% FIGURE 2C (Bonus): percentage of the career where the peak arrives

figure('position',[560   804   279   144]);
hold on;

idx=success_database_artist(:,3)>=3&success_database_artist(:,3)<10;%at least 10 years of success
a=hist(success_database_artist(idx,5)./success_database_artist(idx,3)*100,0:1:100);
h1=plot((0:1:100)-1,cumsum(a)/sum(a)*100,'linewidth',2);

idx=success_database_artist(:,3)>=10&success_database_artist(:,3)<20;%at least 10 years of success
b=hist(success_database_artist(idx,5)./success_database_artist(idx,3)*100,0:1:100);
h1=plot((0:1:100)-1,cumsum(b)/sum(b)*100,'linewidth',2);

idx=success_database_artist(:,3)>=20;%at least 10 years of success
c=hist(success_database_artist(idx,5)./success_database_artist(idx,3)*100,0:5:100);
hold on;
h2=plot(0:5:100,cumsum(c)/sum(c)*100,'linewidth',2);

xlim([0 100])
xlabel('% of career');
ylabel('% artists');
legend({'Career < 10y','10 \leq Success < 20 years','Success \geq 20 years'},'interpreter','tex')
legend('boxoff')


%% FIGURE 3A & B: highest rank and album lifetime in the charts

cmap=lines(10);


%Figure 3A: highest rank reached by an album
figure('position',[500         613         279         183]);
hold on;
clear h
clear mean_arch
for k=1:length(nb_alb)-1,
    clear h
    for j=1:nb_alb(k),
        idx=not(isnan(database_artists_archive(:,nb_alb(k),4)))&isnan(database_artists_archive(:,min(end,nb_alb(k+1)),4));
        plot(rand(sum(idx),1)*0.1+j-0.2+0.1*(k-1),(database_artists_archive(idx,j,4)),'.','color',min(cmap(k,:)+0.2,1),'markersize',3);
        h(j)=errorbar(j,mean((database_artists_archive(idx,j,4))),ste((database_artists_archive(idx,j,4))),'color',cmap(k,:),'capsize',0.4,'linewidth',2);
        mean_arch(k,j)=mean((database_artists_archive(idx,j,4)));
    end;
end;
mean_arch(mean_arch==0)=nan;
for k=1:length(nb_alb)-1,
    plot(mean_arch(k,:),'k-','linewidth',2,'color',cmap(k,:));
end;
set(gca,'ytick',0:40:200,'xtick',1:2:nb_alb(end-1));
xlim([0.5 nb_alb(end-1)+0.5]);
ylim([0 200]);
title('Max Rank of albums in the charts')


%Figure 3B: Lifetime (in weeks) of albums in the charts
figure('position',[800         613         279         183]);
hold on;
clear h

nb_alb=[2 3 5 10 20 200];
clear mean_arch
for k=1:length(nb_alb)-1,
    clear h
    for j=1:nb_alb(k),
        idx=not(isnan(database_artists_archive(:,nb_alb(k),2)))&isnan(database_artists_archive(:,min(end,nb_alb(k+1)),2));
        plot(rand(sum(idx),1)*0.1+j-0.2+0.1*(k-1),log10(database_artists_archive(idx,j,2)),'.','color',min(cmap(k,:)+0.2,1),'markersize',3);
        h(j)=errorbar(j,mean(log10(database_artists_archive(idx,j,2))),ste(log10(database_artists_archive(idx,j,2))),'color',cmap(k,:),'capsize',0.4,'linewidth',2);
        mean_arch(k,j)=mean(log10(database_artists_archive(idx,j,2)));
    end;
end;
mean_arch(mean_arch==0)=nan;
for k=1:length(nb_alb)-1,
    plot(mean_arch(k,:),'k-','linewidth',2,'color',cmap(k,:));
end;
set(gca,'ytick',log10([1 4 16 64 256]),'yticklabel',[1 4 16 64 256],'xtick',1:2:nb_alb(end-1));
xlim([0.5 nb_alb(end-1)+0.5]);
ylim(log10([0.5 512]));
title('Lifetime of albums in the charts')


%Bonus: cumulative score of albums
cmap=lines(10);
figure('position',[1100         613         279         183]);
hold on;
clear h
clear mean_arch
for k=1:length(nb_alb)-1,
    clear h
    for j=1:nb_alb(k),
        idx=not(isnan(database_artists_archive(:,nb_alb(k),5)))&isnan(database_artists_archive(:,min(end,nb_alb(k+1)),5));
        plot(rand(sum(idx),1)*0.1+j-0.2+0.1*(k-1),log10(database_artists_archive(idx,j,5)),'.','color',min(cmap(k,:)+0.2,1),'markersize',3);
        h(j)=errorbar(j,mean(log10(database_artists_archive(idx,j,5))),ste(log10(database_artists_archive(idx,j,5))),'color',cmap(k,:),'capsize',0.4,'linewidth',2);
        mean_arch(k,j)=mean(log10(database_artists_archive(idx,j,5)));
    end;
end;
mean_arch(mean_arch==0)=nan;
for k=1:length(nb_alb)-1,
    plot(mean_arch(k,:),'k-','linewidth',2,'color',cmap(k,:));
end;
set(gca,'ytick',log10([1 10 100 1000 10000]),'yticklabel',[1 10 100 1000 10000],'xtick',1:2:nb_alb(end-1));
xlim([0.5 nb_alb(end-1)+0.5]);
title('Cum Score of albums in the charts')



%% FIGURE 3Cii & Dii: number of weeks in the charts for albums in the TOP 10

% artist='The Rolling Stones';
% artist='Billy Joel';
% artist='Madonna';
% artist='Adele';
% artist='Prince';
% artist='Michael Jackson';
% artist='Barbra Streisand';
artist='Madonna';
% artist='Garth Brooks';
% artist='Mariah Carey';
% artist='The Beatles';
% artist='Bruce Springsteen';
% artist='Stevie Wonder';
% artist='Elton John';
% artist='Bob Dylan';

%identify albums who where number ones
idx=strcmp(table_albums_s(:,2),artist)&score>=190;
albums=unique(table_albums_s(idx,3));

%chronological order
clear debut_year nb_weeks_charts
for i=1:length(albums),
    debut_year(i)=min(Y(strcmp(table_albums_s(:,2),artist)&strcmp(table_albums_s(:,3),albums{i})));
end;
[~,sorty]=sort(debut_year);

figure('position',[560   803   217   145]);
hold on;
for i=1:length(albums),
    idx=strcmp(table_albums_s(:,2),artist)&strcmp(table_albums_s(:,3),albums{i});
    nb_weeks_charts(i)=sum(idx);
    max_score(i)=max(score(idx));
    
    if max_score(i)==200,
        bar(debut_year(i),nb_weeks_charts(i),'edgecolor','none','facecolor',0*[1 1 1]);
    else
        bar(debut_year(i),nb_weeks_charts(i),'edgecolor','none','facecolor',0.7*[1 1 1]);
    end;
end;
set(gca,'xtick',[1963 1980:20:2020]);
xlabel('years');
ylabel('nb weeks');
% xlim([1962 2020]);%for Barbra Streisand
xlim([1980 2020]);%for Madonna


%% FIGURE 3E: plot lifetime of an album in the charts as a function of the year

figure('position',[560   100   240   160]);
hold on;

clear h
temp=reshape(database_artists_archive(:,:,1:2),size(database_artists_archive,1)*size(database_artists_archive,2),2);
temp=temp(~isnan(temp(:,1))&temp(:,1)>1,:);
plot(temp(:,1)/52+1963,temp(:,2),'.','color',[0.6 0.6 1],'markersize',3);

%non parametric regression
[f,newx,abscisse] = regnp(temp(:,1)/52+1963,log10(temp(:,2)),2,100,0);
plot(abscisse,10.^f,'color',[0.85 0.33 0.1],'linewidth',2);

set(gca,'ytick',([1 6 13 26 52 104 208]),'yticklabel',[1 6 13 26 52 104 208],'yscale','log','xtick',[1963 1980 2000 2020]);
ylim(([0.5 512]));
title('Lifetime of albums in the charts')

%% FIGURE 3F: plot number of albums in the charts as a function of the year
clear n_album_peryear
for y=1963:2019,
    y
    idx2=find(Y==y);
    idx=find(Y<y);
    all_albums=table_albums_s(idx2,3);
    [all_albums,ia,ic]=unique(all_albums);
    idx_unique_2000=idx2(ia);
    [all_albums,ia2] =setdiff(all_albums,unique(table_albums_s(idx,3)));
    idx_unique_2000=idx_unique_2000(ia2);
    n_album_peryear(y)=length(idx_unique_2000);
end;

figure('position',[560   100   240   160]);
hold on;

bar(1963:2019,n_album_peryear(1963:2019),'facecolor',[0.6 0.6 1],'edgecolor','none');

set(gca,'xtick',[1963 1980 2000 2020]);

title('# albums in the charts')


%% FIGURE 4A: ranking evolution of Coldplay albums

%sales: extracted data from image (Fig 2 in Jarynowski and Buda 2004)
%authors made a mistake, it was not Bruce Springsteen's albums, I found out
%because of album release dates

artist='Coldplay';
albums={'X&Y','Viva La Vida or Death And All His Friends'};

load('Coldplay_sales_data');%data is in Data001
idx_coldplay{1}=2:29;%time index in Data001 for each Coldplay album
idx_coldplay{2}=32:60;

figure('position',[560   719   662   229]);

for j=1:length(albums),
    subplot(1,2,j);
    hold on;
    %extract ranking
    idx=strcmp(table_albums_s(:,2),artist)&strcmp(table_albums_s(:,3),albums{j});
    
    distrib_weeks=accumarray([D(idx) M(idx) Y(idx)],score(idx),[],@sum);
    if length(distrib_weeks)<2019,
        distrib_weeks(:,:,2019)=0;
    end;
    if size(distrib_weeks,2)<12,
        distrib_weeks(:,12,:)=0;
    end;
    distrib_weeks=distrib_weeks(:,:,1963:2019);
    distrib_weeks=distrib_weeks(:);
    
    idx_p=find(distrib_weeks,1);
    distrib_weeks=distrib_weeks(idx_p:end);
    distrib_weeks(distrib_weeks==0)=nan;
    yyaxis left
    plot(distrib_weeks,'.');
    
    %superimpose with sales
    yyaxis right
    plot((Data001(idx_coldplay{j},1)-Data001(idx_coldplay{j}(1),1))*7,Data001(idx_coldplay{j},2));
    ylim([0 2000])
    set(gca,'yscale','linear')
    title(albums{j})
    set(gca,'xtick',0:200:1000);
    xlabel('days');
    xlim([0 600]);
end;

%% FIGURE 4B : model rank evolution with time

%read excel file of weekly charts in korea
[database_an,database_txt]=xlsread('heanteo_charts.xlsx');

sales=database_an';

x1=1:30;%first 30 albums ranked
y1=mean(sales);

%power law fitting
modelfun_inverse = @(b,x)(b(1)./(x(:).^b(2)));

[beta,R,J,CovB,MSE,ErrorModelInfo] = nlinfit(x1(:),y1(:),modelfun_inverse,[y1(1) 1.75],'weights',(1:30)'/30);%[ones(5,1);1*ones(n_years-5,1)]);

figure('position',[528        200         302         226]);
%individual curves
plot(sales','color',0.8*[1 1 1])
hold on;

%average and STD
errorbar(1:30,mean(sales),zeros(1,30),std(sales),'color','k','linewidth',1,'capsize',3);
plot(mean(sales),'k-','linewidth',2)

%power law fit
plot(x1,modelfun_inverse(beta,x1),'r:','linewidth',2);

set(gca,'box','off','yscale','log','ytick',10.^(3:6),'xtick',[1 10:10:30])
xlabel('Rank');
ylabel('Sales');
xlim([0.5 31])

%% FIGURE 4C: simulate two ranking evolutions

figure('position',[1677         996         266         191]);
hold on;

%score
yyaxis left

%the album starts at 2 times the sales of the following one
VA1_X=1/2;
%fit from Hanteo chart
a=1.6742%beta(2) in previous section

%time index in days, one point per week
time_days=1:7:600;

plot(time_days,201-floor((VA1_X*exp((1:7:600)/60)).^(1/a)),'.')
plot(time_days,201-floor((VA1_X*exp((1:7:600)/30)).^(1/a)),'.r')

ylim([0 200])
ylabel('score');

%the other scale: the ranking
yyaxis right
ylim([1 200])
set(gca,'ytick',[1 50 100 150 200],'ydir','reverse')

xlabel('days');
ylabel('ranking');
set(gca,'box','off')


%% FIGURE 4D: ranking evolution (by week) for a set of albums for a given artist

% artist='The Rolling Stones';
% artist='Billy Joel';
% artist='Madonna';
% artist='Adele';
% artist='Prince';
artist='Michael Jackson';
% artist='Barbara Streisand';
% artist='Garth Brooks';
% artist='Mariah Carey';
% artist='The Beatles';
% artist='Bruce Springsteen';
% artist='Stevie Wonder';
albums={'Off The Wall','Thriller','Bad','Dangerous','HIStory: Past, Present And Future Book 1','Invincible'};

years_ref=1963:2019;


% figure('position',[560   787   273   161]);%true dimensions in the paper
figure('position',[560   787   532   161]);
hold on;
title(artist)
for j=1:length(albums),
    idx=strcmp(table_albums_s(:,2),artist)&strcmp(table_albums_s(:,3),albums{j});
    
    distrib_weeks=accumarray([D(idx) M(idx) Y(idx)],score(idx),[],@sum);
    if length(distrib_weeks)<2019,
        distrib_weeks(:,:,2019)=0;
    end;
    if size(distrib_weeks,2)<12,
        distrib_weeks(:,12,:)=0;
    end;
    distrib_weeks=distrib_weeks(:,:,1963:2019);
    distrib_weeks=distrib_weeks(:);
    
    idx_p=find(distrib_weeks,1);
    distrib_weeks=distrib_weeks(idx_p:end);
    distrib_weeks(distrib_weeks==0)=nan;
    plot(distrib_weeks,'.');
end;

legend(albums,'location','eastoutside')
legend('boxoff')

set(gca,'xtick',0:200:1000);
xlabel('days');
ylabel('score');
xlim([0 1000]);


%% FIGURE 4E: time between two ranked albums as a function of years

idx=success_weeks_interv_arch<=40*52&success_weeks_interv_arch>0;
figure('position',[560   100   240   160]);

%plot individual points
plot(success_weeks_arch(idx)/52+1963+(rand(size(success_weeks_interv_arch(idx)))-0.5)*0.4,success_weeks_interv_arch(idx)/52+(rand(size(success_weeks_interv_arch(idx)))-0.5)*0.4/52,'.','markersize',2,'color',[0.6 0.6 1]);
hold on;

%non parametric regression
years_interv=unique(success_weeks_arch(idx));
[f,newx,abscisse] = regnp(success_weeks_arch(idx)/52+1963,log10(success_weeks_interv_arch(idx)/52),2,100,0);
plot(abscisse,10.^f,'color',[0.85 0.33 0.1],'linewidth',2);

clear prc_y mean_y
%prc by segments
for y=1964:2019,
    idx_y=success_weeks_arch/52+1963>y-1&success_weeks_arch/52+1963<y+1;
    prc_y(y,:)=prctile((success_weeks_interv_arch(idx_y&idx)/52),[10 90]);
    mean_y(y)=mean(log10(success_weeks_interv_arch(idx_y&idx)/52));
end;
plot(1964:2019,prc_y(1964:2019,:),'--','color','r');

set(gca,'xtick',[1963 1980 2000 2020])
xlabel('year');
ylabel('years between 2 albums');
set(gca,'yscale','log','ytick',[0.5 1 2 5 10 20 40],'box','off')
ylim([0.1 40])

%% FIGURE 4F: rank evolution of an album (relative to the previous one) as a function of the time in years with the previous album

%group data for large time intervals between albums
temp=success_years_interv_arch;
success_years_interv_arch2=temp;
success_years_interv_arch2(temp>=8&temp<10)=9;
success_years_interv_arch2(temp>=10&temp<15)=12.5;
success_years_interv_arch2(temp>=15&temp<20)=17.5;
success_years_interv_arch2(temp>=20&temp<25)=22.5;
success_years_interv_arch2(temp>=25)=27.5;

figure('position',[560   804   279   144]);
plot(success_years_interv_arch2+(rand(size(success_years_interv_arch2))-0.5)*0.4,log10(score_success_ref_arch),'.','markersize',2,'color',[0.6 0.6 1]);
hold on;
plot([0 30],0*[1 1],'k--');
years_interv=unique(success_years_interv_arch2);
[mean_grp,std_grp,ste_grp,n_grp]=grpstats(log10(score_success_ref_arch),success_years_interv_arch2,{'mean','std','sem','numel'});
errorbar(years_interv,mean_grp,ste_grp,'capsize',3,'linewidth',2,'color',[0.85 0.33 0.1]);
ylim([log10(1/5) log10(1.2)])
set(gca,'ytick',[log10(1/5) log10(0.5) log10(0.8) 0 log10(1.2)],'yticklabel',{'-80','-50','-20','0','+20'},'box','off')

%% SUPP FIGURE 3: success for a set of artists - BILLBOARD 100

% artist='The Rolling Stones';
% artist='Billy Joel';
% artist='Madonna';
% artist='Adele';
% artist='Prince';
% artist='Michael Jackson';
% artist='Barbara Streisand';
% artist='Garth Brooks';
% artist='Mariah Carey';
% artist='The Beatles';
% artist='Bruce Springsteen';
% artist='Stevie Wonder';
% artist='John Coltrane';
% artist={'The Rolling Stones'};
artist={'Bob Dylan','Barbra Streisand','Elton John','Billy Joel','Garth Brooks','Mariah Carey'};

figure('position',[560   796   345   152]);
hold on;

for j=1:length(artist),
    idx=strcmp(table_albums_s_100(:,2),artist{j});
    distrib_year=accumarray(Y_100(idx),score_100(idx),[],@sum);
    if length(distrib_year)<2019,
        distrib_year(2019)=0;
    end;
    distrib_year=distrib_year(1963:2019);
    
    y=smoothbins(distrib_year,5);
    y(y==0)=nan;
    plot(1963:2019,y,'linewidth',2)
end;
set(gca,'xtick',[1963 1980 2000 2020])
legend(artist)
legend('boxoff')
xlabel('year');
ylabel('score');

%% SUPP FIGURE 4A: distance in weeks with the previous album as a function of the Nth album ranked for an artist

nb_albums_per_artist=accumarray(artist_weeks_arch(:,1),artist_weeks_arch(:,2),[],@max);
maxi=max(nb_albums_per_artist);

%we select albums less than forty years between from each other
%and we select artists who placed more than 10 albums in the charts
idx=success_weeks_interv_arch<=40*52&success_weeks_interv_arch>0&ismember(artist_weeks_arch(:,1),find(nb_albums_per_artist>=10));

figure('position',[560   100   240   160]);
plot(artist_weeks_arch(idx,2)+(rand(size(success_weeks_interv_arch(idx)))-0.5)*0.4,success_weeks_interv_arch(idx)/52+(rand(size(success_weeks_interv_arch(idx)))-0.5)*0.4/52,'.','markersize',2,'color',[0.6 0.6 1]);
hold on;
%non parametric regression
[f,newx,abscisse] = regnp(artist_weeks_arch(idx,2),log10(success_weeks_interv_arch(idx)/52),1.5,100,0);
plot(abscisse,10.^f,'color',[0.85 0.33 0.1],'linewidth',2);

clear prc_y mean_y
%percentile by segments
for y=1:maxi,
    idx_y=artist_weeks_arch(:,2)==y;
    prc_y(y,:)=prctile((success_weeks_interv_arch(idx_y&idx)/52),[10 90]);
    mean_y(y)=median(log10(success_weeks_interv_arch(idx_y&idx)/52));
end;
plot(1:maxi,prc_y(1:maxi,:),'--','color','r');

xlabel('# album');
ylabel('years between 2 albums');
set(gca,'yscale','log','ytick',[0.5 1 2 5 10 20 40],'box','off')
ylim([0.1 40])
xlim([0 60])



%% SUPP FIGURE 4Bi & Bii: simulate a career (very preliminary)

N0=20;%career of N0 albums
N=1:N0;%each album index

%years between two albums: constant value according to SUPP FIGURE 4
time_inter_album=1;
%time in weeks of album publishing
time_albums=0:52*time_inter_album:N0*52*time_inter_album;

time=0:time_albums(end)+2*52;%index all weeks+2 years

%generate max score reached by the artist
maxi_score=min(200,85+20*sqrt(N0));%numbers determined by a linear regression on FIGURE3A
mini_score=min(122,80+N0*8);

%generate max scores for each album
%beta distribution
alpha=3;
beta=5;%these parameters give a peak at 1/3 of the career according to FIGURE 3A
betapdf=(N/N0).^(alpha-1).*(1-N/N0).^(beta-1);
betapdf=betapdf/max(betapdf)*(maxi_score-mini_score)+mini_score;
% figure;plot(N,betapdf,'-o')

%generate duration in the charts for each album
%t0 in ranking(t);
time_constant=(1.5*N0+5-1.3*N);%numbers determined by a linear regression on FIGURE3B
%adjust time constant so that an averaged-rank album still lasts time_constant
%(the current ranking(t) model does not account for progressive decline in lifetime duration for an album
%whatever the rhighest rank achieved. 
%We do that by roughly adjusting the time constant. To further improve)
time_constant=time_constant.*(50./betapdf+1/4);

score_week=zeros(size(time));
for alb=1:N0,
    %rank evolution of N-th album
    idx_alb=(time_albums(alb)+(0:52*2));
    score_week(idx_alb+1)=score_week(idx_alb+1)+max(0,201-ceil((201-betapdf(alb))*exp((0:52*2)/time_constant(alb)/1.6742)));
end;

%Supp. Figure 4Bi: score per week
figure('position',[200   800   356   145]);
hold on;
plot(time,score_week)
xlabel('week');
ylabel('score');
xlim([0 52*22])

%Supp. Figure 4Bii: cumulate score per year
figure('position',[500   800   356   145]);
hold on;
bar(unique(floor(time/52))+0.5,accumarray(floor(time(:)/52)+1,score_week(:),[],@sum),'facecolor',[0.6 0.6 1],'edgecolor','none')
xlabel('year');
ylabel('cumulated score');
xlim([0 22])