
---

Matlab Code for analysis of Billboard 200 database of albums (1963-2019)
Compatible with Matlab 2019b and later, not tested before but should work
Open analyze_dB_BB200.m and run sections
All database studied can also be downloaded from this repository

Gourévitch, "Billboard 200: The lessons of musical success in the US", Music and Science, 2023 in press

Contact: boris@pi314.net

---
